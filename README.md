# Gitlab Pages example

This is an example for Gitlab pages. You can find the resulting documentation [here](http://julien.kindle.gitlab.io/gitlab-pages-example-gitbook/).

In the file [.gitlab-ci.yml](https://gitlab.com/julien.kindle/gitlab-pages-example-gitbook/-/blob/master/.gitlab-ci.yml), we define how to build HTML pages from the markdown source. Then, the CI generates the HTML files and saves them as "artifact".

**These artifacts are then served via an HTTP server at [http://julien.kindle.gitlab.io/gitlab-pages-example-gitbook/](http://julien.kindle.gitlab.io/gitlab-pages-example-gitbook/).**

The output of the CI pipeline can be viewed [here](https://gitlab.com/julien.kindle/gitlab-pages-example-gitbook/-/pipelines/361648133).
